from datetime import timedelta
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect, render
from django.contrib.auth import logout, login, authenticate
from django.utils import timezone
from main.models import User
# Create your views here.


def home(request):
    return render(request, 'home.html', {})


def log_out(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect("/login")


def login_view(request):
    if request.user.is_authenticated:
        subscription_time = request.user.subscription_date
        if subscription_time < timezone.now():
           user = User.objects.filter(username=request.user.username)
           user.update(bought_subscription=False)
        return redirect("/")
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        check = authenticate(username=username, password=password)
        if check:
            login(request, check)
            return redirect("/dashboard")
        return HttpResponse("Something went Error...")
    return render(request, 'login.html', {})


def dashboard(request):
    if not request.user.is_authenticated:
        return HttpResponse("You are not Logged in")
    subscription_time = request.user.subscription_date
    time = (subscription_time-timezone.now()).days
    return render(request, 'dashboard.html', {"time": time})


def buy_30days_sub(request):
    if request.method == "POST":
        if request.user.is_authenticated:
            if request.user.bought_subscription == True:
                return HttpResponse("You Bought 30 Days Subscription before...")
            user = User.objects.filter(username=request.user.username)
            user.update(bought_subscription=True)
            time = user.first().subscription_date
            user.update(free_trial=False)
            user.update(subscription_date=time + timedelta(days=30))
            return HttpResponse("You bought it successfuly")
        return redirect("/login")
    return HttpResponseBadRequest("GET is not Allowed")
