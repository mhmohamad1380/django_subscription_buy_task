from datetime import timedelta
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

def get_free_trial_time():
    now = timezone.now()
    return now + timedelta(days=30)

class User(AbstractUser):
    free_trial = models.BooleanField(default=True)
    free_trial_time = models.DateTimeField(default=get_free_trial_time)
    bought_subscription = models.BooleanField(default=False)
    subscription_date = models.DateTimeField(default=timezone.now)