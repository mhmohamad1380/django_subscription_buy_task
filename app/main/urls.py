from django.urls import path
from . import views



urlpatterns = [
    path("",views.home),
    path("logout",views.log_out),
    path("login",views.login_view),
    path("dashboard",views.dashboard),
    path("buy-30days-sub",views.buy_30days_sub),
]